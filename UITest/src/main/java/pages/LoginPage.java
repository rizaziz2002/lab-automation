package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class LoginPage {


    public LoginPage(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    @FindBy(name = "user")
    public WebElement fldUsername;

    @FindBy(name = "pass")
    public WebElement fldPassword;

    @FindBy(xpath= "//input[@value='Login']")
    public WebElement btnLogin;

    @FindBy(linkText = "Items page")
    public WebElement lnkItemsPage;

    @FindBy(tagName = "a")
    public List<WebElement> downloadLinks;


    public void login(String username, String password){
        this.fldUsername.sendKeys(username);
        this.fldPassword.sendKeys(password);
        this.btnLogin.click();
    }



}

package test;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.Assertion;
import pages.LoginPage;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;

public class LoginTest {

    private LoginPage loginPage;
    private WebDriver driver;

    @BeforeClass
    public void setUp(){
        WebDriverManager.chromedriver().setup();
        this.driver = new ChromeDriver();
        this.loginPage = new LoginPage(driver);
    }


    @Test
    public void loginTest(){

        driver.get("http://45.76.1.18:5000/login");
        this.loginPage.login("admin", "admin1234");

        this.loginPage.lnkItemsPage.click();

        this.loginPage.downloadLinks.get(1).click();

        String path = "/users/azizjonrizayev/downloads/item.txt";

        FileReader fr = null;
        BufferedReader br = null;

        String fileContent=null;

        try{
            fr =new FileReader(path);
            br = new BufferedReader(fr);
            String line;
            StringBuilder sb = new StringBuilder();
            while((line = br.readLine()) != null){
                sb.append(line);
            }

            fileContent = sb.toString();

        }catch(IOException e){
            e.printStackTrace();
        }


        Assert.assertEquals(fileContent, "qyntalvpoxmyfbtb (yes, this was the second item)");

    }
}
